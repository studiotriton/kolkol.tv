/**
 * Created by hramik on 23/09/15.
 */
$(document).ready(function () {

    $('.ui.embed').embed();

    $('.ui.item_item.dropdown').dropdown({
        on: 'hover',
        duration   : 200,
    });

    $('.slideshow').slick({
        dots: true,
        lazyLoad: 'ondemand',
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        adaptiveHeight: true
    });


    $('.news_slideshow').slick({
        dots: true,
        lazyLoad: 'ondemand',
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        adaptiveHeight: true,
        autoplay: true,
        autoplaySpeed: 3000
    });

    //$('.videos').slick({
    //    dots: true,
    //    lazyLoad: 'ondemand',
    //    infinite: true,
    //    speed: 300,
    //    slidesToShow: 3,
    //    slidesToScroll: 3
    //});

});
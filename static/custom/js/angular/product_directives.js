/**
 * Created by hramik on 29/10/14.
 */

'use strict';


angular.module('tritonProductDirectives', [])
    .directive('product', function ($compile) {
        return {
            restrict: 'E',
            controller: function ($scope, $element, $location, $http, Global) {
                $scope.global = Global;


                $scope.$watch('global.current_product_menu', function (newValue, oldValue) {
                    console.log(newValue);
                    $('.product_thumbs').attr('tab', newValue);
                })

                $scope.filter = function (group_name, filter_name) {
                    return true;
                }

                $scope.menu_click = function (menu_name) {
                    console.log("out from ", menu_name);
                    $scope.global.current_product_menu = menu_name;
                    product_video.pause();
                }

                $scope.bigger3D = function () {
                    $('#ModelViewerInner').attr('width', '500px').attr('height', '450px');
                }

                $scope.smaller3D = function () {
                    $('#ModelViewerInner').attr('width', '200px').attr('height', '150px');
                }

                $scope.get_current_price = function () {
                    $http.get('http://' + $location.host() + ":" + $location.port() + '/get_current_price/' + $scope.id).success(function (data) {
                        $scope.current_price = data;
                        $scope.cur_price = data.cart_sum;
                        return data.cart_sum;
                    });
                }

                $scope.addCart = function () {
                    $http.get('http://' + $location.host() + ":" + $location.port() + '/add_to_cart/' + $scope.id).success(function (result) {
                        if (result == 'True') {
                            $http.get('http://' + $location.host() + ":" + $location.port() + '/get_cart').success(function (data) {
                                $scope.global.cart = data;
                                $scope.cur_price = data;
                            });
                        }
                    });
                }

                $scope.deleteFromCart = function () {
                    $http.get('http://' + $location.host() + ":" + $location.port() + '/delete_from_cart/' + $scope.id).success(function (result) {
                        if (result == 'True') {
                            $http.get('http://' + $location.host() + ":" + $location.port() + '/get_cart').success(function (data) {
                                $scope.global.cart = data;
                            });
                        }
                    });
                }

                $scope.clearAllOptions = function (product_id) {
                    $http.get('http://' + $location.host() + ":" + $location.port() + '/clear_all_option_from_session/' + product_id).success(function (result) {
                        if (result == 'True') {
                            $http.get('http://' + $location.host() + ":" + $location.port() + '/get_cart').success(function (data) {
                                $scope.global.cart = data;
                            });
                            $http.get('http://' + $location.host() + ":" + $location.port() + '/get_current_price/' + product_id).success(function (data) {
                                $scope.global.current_price = data;
                            });
                        }
                    });
                }


                $scope.addFavorite = function () {
                    $http.get('http://' + $location.host() + ":" + $location.port() + '/add_to_favorite/' + $scope.id).success(function (result) {
                        if (result == 'True') {
                        }
                    });
                }

                $scope.deleteFavorite = function () {
                    $http.get('http://' + $location.host() + ":" + $location.port() + '/delete_from_favorite/' + $scope.id).success(function (result) {
                        if (result == 'True') {
                        }
                    });
                }

                $scope.addComparsion = function () {
                    $http.get('http://' + $location.host() + ":" + $location.port() + '/add_to_comparsion/' + $scope.id).success(function (result) {
                        if (result == 'True') {
                        }
                    });
                }

                $scope.deleteComparsion = function () {
                    $http.get('http://' + $location.host() + ":" + $location.port() + '/delete_from_copmarsion/' + $scope.id).success(function (result) {
                        if (result == 'True') {
                        }
                    });
                }


            }
        };
    });
//    .directive('menuitem', function () {
//        return {
//            require: '^menu',
//            restrict: 'E',
//            transclude: true,
//            scope: {
//                href: '@',
//                rel: '@'
//            },
//            link: function ($scope, $element, $attrs, da_menuCtrl) {
//                da_menuCtrl.addMenuItem($scope);
//            },
////            template: '<li class="menu_item"  ng-class="{active:selected}"><a ng-transclude></a></li>',
//            template: '<li class="menu_item"><a ng-transclude></a></li>',
//            replace: true
//        };
//    });
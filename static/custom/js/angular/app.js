'use strict';

var kprf_app = angular.module('kolokolApp',
    [
        'ngAnimate',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'kprfServices',
        'kprfControllers',
        'kprfDirectives',
        'yaMap'
    ],
    function ($interpolateProvider, $resourceProvider, $locationProvider) {
        $interpolateProvider.startSymbol('{?');
        $interpolateProvider.endSymbol('?}');
        $resourceProvider.defaults.stripTrailingSlashes = false;
        //$locationProvider.html5Mode({
        //    enabled: true,
        //    requireBase: false
        //});
    });


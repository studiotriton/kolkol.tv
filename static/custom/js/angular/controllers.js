'use strict';

//var API_URL = 'http://ayvazovsky.webfactional.com/';
var API_URL = 'http://127.0.0.1:8000/';
//var API_URL = 'http://triton_ua.in.ua/';
//var API_URL = 'http://triton_ua.webfactional.com/';
var PORT = ':8000';
//var PORT = '';

angular.module('kprfControllers', ['ngResource'])
    .controller('appCtrl', function ($scope, Global, $http, $location, $interval) {
        $scope.global = Global;
    })
    .controller('formCtrl', function ($scope, Global, $http, $location) {
        $scope.global = Global;

        $scope.send_form = function () {
            //console.log($scope, $scope.id, $scope.$parent.items);
            try {
                console.log(call_value);
                var callvalue = call_value;
            } catch (err) {
                console.log(err);
                var callvalue = '';
            }

            $http({
                method: 'POST',
                url: 'http://' + $location.host() + ":" + $location.port() + '/send_form/' + $scope.id,
                data: {
                    'items': $scope.$parent.items,
                    'call_value': callvalue
                },
                headers: {'Content-Type': 'application/json'}
            }).success(function (data) {
                console.log(data);
                $scope.loading = 0;
                $scope.success = 1;

            });
        }
    });
